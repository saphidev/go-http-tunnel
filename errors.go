// Copyright (C) 2019 Safi Eddine
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package tunnel

import "errors"

var (
	errClientNotSubscribed    = errors.New("client not subscribed")
	errClientNotConnected     = errors.New("client not connected")
	errClientAlreadyConnected = errors.New("client already connected")

	errUnauthorised = errors.New("unauthorised")
)
